# Quick install guide

Install nvm from here:
```
https://github.com/creationix/nvm
```

Install npm dependencies:
```
npm install
```

Install global requirements:
```
sudo npm install -g gulp-cli pug
```

Run gulp to start the app
```
gulp
```