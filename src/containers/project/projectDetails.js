import   React                from 'react'
import { connect }            from 'react-redux'
import { Link,
         withRouter }         from 'react-router-dom'
import { setLogoVisibility,
         setHintVisibility,
         setLogoColor,
         setHintColor,
         setLogoState,
         setSidebarState,
         enableProjectDetailsEventListeners }      from 'actions/sidebar'
import { disableRerendering }  from 'actions/projectDetails'
import { lazyLoadVideo }       from 'common/is-visible'
import   projectDefsList       from 'common/projectDefs'
import   colors                from 'common/colors'
import   PDComponent           from 'components/project/projectDetails'

class ProjectDetails extends React.Component{
  componentWillMount(){
    this.project = projectDefsList.find(
      project => project.slug === this.props.match.params.slug
      );
    this.domStore = {}

    this.props.showSidebarLogo(true);
    this.props.showSidebarHint(true);
    this.props.setLogoState('back');
    this.props.setSidebarState('project');

    if (this.project.theme === 'dark') {
      this.props.colorSidebarLogo(colors.light)
      this.props.colorSidebarHint(colors.light)
    } else {
      this.props.colorSidebarLogo(colors.dark)
      this.props.colorSidebarHint(colors.dark)
    }
  }
  componentDidMount(){
    this.buildDomStore();
    this.pinContentToBottom();

    lazyLoadVideo(document.querySelectorAll('[data-lazy-video]'));
    this.addProjectDetailsEventListeners();
  }

  addProjectDetailsEventListeners() {
    window.addEventListener(
      'resize',
      this.pinContentToBottom
      );
    this.domStore.projectContent.addEventListener(
      'scroll',
      this.handleSidebarHint
      );
    this.domStore.projectContent.addEventListener(
      'scroll',
      this.parallaxHero
      );
  }

  removeProjectDetailsEventListeners() {
    window.removeEventListener(
      'resize',
      this.pinContentToBottom
      );
    this.domStore.projectContent.removeEventListener(
      'scroll',
      this.handleSidebarHint
      );
    this.domStore.projectContent.removeEventListener(
      'scroll',
      this.parallaxHero
      );
  }

  buildDomStore = () => {
    this.domStore.projectHero    = document.querySelector('.project-hero')
    this.domStore.projectContent = document.querySelector('.project-body-content')
    this.domStore.projectMeta    = document.querySelector('.project-body-meta-container')
    this.domStore.projectText    = document.querySelector('.project-body-text')
  }

  pinContentToBottom = () => {
    window.requestAnimationFrame( () => {
      this.buildDomStore();
      let projectMeta = this.domStore.projectMeta,
          projectText = this.domStore.projectText,
          windowTooSmall = projectMeta.offsetHeight + 100 > window.innerHeight,
          windowTooLarge = window.innerHeight - projectMeta.offsetHeight > 700

      if (!windowTooSmall && !windowTooLarge) {
        projectText.style.top = window.innerHeight - projectMeta.offsetHeight + 'px'
      } else if (windowTooLarge) {
        projectText.style.top = '700px'
      } else {
        projectText.style.top = null
      }
    })
  }

  parallaxHero = () => {
    var heroEl = this.domStore.projectHero;
    window.requestAnimationFrame( () => {
      var scroll = this.domStore.projectContent.scrollTop;
      heroEl.style.transform = 'translateY(' + scroll * .2 + 'px)';
    })
  }

  // TODO: refactor to sidebar component
  handleSidebarHint = () => {
    window.requestAnimationFrame( () => {
      if(
          this.domStore.projectContent.scrollHeight -
          this.domStore.projectContent.scrollTop <= window.innerHeight * 1.25
          ){
        this.props.showSidebarHint(false);
      } else {
        this.props.showSidebarHint(true);
      }

    })
  }

  render(){
    if (this.props.projectDetailEventListenersEnabled === false) {
      this.removeProjectDetailsEventListeners();
      this.props.enableEventListenersForFutureAttempts();
    }
    if (this.props.rerenderOnViewportChanged === true) {
      window.scrollTo(0, 0);
      this.props.disableRerendering();
    }
    return <PDComponent project={ this.project } />
  }
}

const mapStateToProps = (state, ownProps) => ({
  projectDetailEventListenersEnabled:
    state.sidebar.projectDetailEventListenersEnabled,
  rerenderOnViewportChanged:
    state.projectDetails.rerenderOnViewportChanged,
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  showSidebarLogo: (visibility) => dispatch(
    setLogoVisibility(visibility)
    ),
  showSidebarHint: (visibility) => dispatch(
    setHintVisibility(visibility)
    ),
  colorSidebarLogo: (color) => dispatch(
    setLogoColor(color)
    ),
  colorSidebarHint: (color) => dispatch(
    setHintColor(color)
    ),
  setLogoState: (state) => dispatch(
    setLogoState(state)
    ),
  enableEventListenersForFutureAttempts: () => dispatch(
    enableProjectDetailsEventListeners()
  ),
  disableRerendering: () => dispatch(
    disableRerendering()
  ),
  setSidebarState: (state) => dispatch(
    setSidebarState(state)
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(withRouter(ProjectDetails))