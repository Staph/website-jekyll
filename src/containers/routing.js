import { connect }                        from 'react-redux'
import { withRouter }                     from 'react-router-dom'
import { 
    setLogoState,
    disableHomeEventListeners,
    disableProjectDetailsEventListeners, } from 'actions/sidebar'
import {
    rerenderProjectPageWithNewViewPort }  from  'actions/projectDetails'
import Routing                            from 'components/routing'

function changeStateOnTransitionEnd(
    previousLocation, newLocation, dispatch) {
  // Triger action based on previous and current location
  if (previousLocation.includes('/project/') &&
      newLocation === '/') {
    // if transition was made from project page to main page
    dispatch(
      setLogoState('default'));
    dispatch(
      disableProjectDetailsEventListeners())
  }
  if (newLocation.includes('/project/') &&
      previousLocation === '/') {
    // if transaction was made from main page to project page
    dispatch(disableHomeEventListeners())
  }
}

function changeStateOnTransitionExited(
    previousLocation, newLocation, dispatch) {
  // Triger action based on previous and current location
  if (newLocation.includes('/project/') &&
      previousLocation === '/') {
    // if transaction was made from main page to project page
    // rerender project page with new viewport
    dispatch(rerenderProjectPageWithNewViewPort())
  }
}


const mapDispatchToProps = (dispatch, ownProps) => ({
  onCssTransitionExiting: (previousLocation, newLocation) => 
    changeStateOnTransitionEnd(
      previousLocation, newLocation, dispatch),
  onCssTransitionExited: (previousLocation, newLocation) => 
    changeStateOnTransitionExited(
      previousLocation, newLocation, dispatch)
})

//export default Routing
export default withRouter(connect(
  null,
  mapDispatchToProps
)(Routing))