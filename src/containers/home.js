import   React                      from 'react'
import { connect }                  from 'react-redux'
import   colors                     from 'common/colors'
import { setLogoVisibility,
         setHintVisibility,
         setLogoColor,
         setHintColor,
         setLogoState,
         setSidebarState,
         enableHomeEventListeners } from 'actions/sidebar'
import   HomeComponent              from 'components/home'

class Home extends React.Component {
  componentDidMount(){
    this.domStore = {};
    this.storeDomElements();
    this.props.initAnimation();

    window.scrollTo( 0, this.props.home.scrollTop );
    this.addHomeEventListeners()

    this.handleSidebar();
    this.props.setSidebarState('home');
  }

  addHomeEventListeners() {
    window.addEventListener('scroll', this.handleSidebar );
  }

  removeHomeEventListeners() {
    window.removeEventListener('scroll', this.handleSidebar )
  }

  storeDomElements = () => {
    this.domStore.header       = document.querySelector('#header');
    this.domStore.footer       = document.querySelector('#footer');
    this.domStore.sidebarLogo  = document.querySelector("#sidebar-logo");
    this.domStore.sidebarHint  = document.querySelector("#sidebar-hint-text");
    this.domStore.darkElements = document.querySelectorAll(".theme-dark");
  }

  handleSidebarHint = () => {
    window.requestAnimationFrame( () => {
      var footerBox = this.domStore.footer.getBoundingClientRect(),
          overlap = false;

      if( this.props.options.initialLoad == true || footerBox.y <= 0 ){
        this.props.showSidebarHint(false);
      } else {
        this.props.showSidebarHint(true);
      }

      overlap = this.props.checkOverlap(
        this.domStore.sidebarHint,
        this.domStore.darkElements
        );

      if ( overlap == true ) {
        this.props.colorSidebarHint(colors.light)
      } else {
        this.props.colorSidebarHint(colors.dark)
      }
    })
  }
  handleSidebarLogo = () => {
    window.requestAnimationFrame( () => {
      var headerBox = this.domStore.header.getBoundingClientRect(),
          overlap   = false;

      if( headerBox.top*-1 > headerBox.height/2 ){
        this.props.showSidebarLogo(true);
      } else {
        this.props.showSidebarLogo(false);
      }

      if( headerBox.top*-1 > headerBox.height ){
        this.props.setLogoState('up')
      } else {
        this.props.setLogoState('default')
      }

      overlap = this.props.checkOverlap(
        this.domStore.sidebarLogo,
        this.domStore.darkElements
        );

      if ( overlap == true ) {
        this.props.colorSidebarLogo(colors.light)
      } else {
        this.props.colorSidebarLogo(colors.dark)
      }

    })
  }
  handleSidebar = () => {
    this.handleSidebarHint()
    this.handleSidebarLogo()
  }

  render(){
    if (this.props.homeEventListenersEnabled === false) {
      this.removeHomeEventListeners();
      this.props.enableEventListenersForFutureAttempts();
    }
    return (
      <HomeComponent initialLoad={this.props.initialLoad}
                     setHomeScroll={this.props.setHomeScroll} />
      )
  }
}

const mapStateToProps = (state, ownProps) => ({
  homeEventListenersEnabled:
    state.sidebar.homeEventListenersEnabled
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  showSidebarLogo: (visibility) => dispatch(
    setLogoVisibility(visibility)
    ),
  showSidebarHint: (visibility) => dispatch(
    setHintVisibility(visibility)
    ),
  colorSidebarLogo: (color) => dispatch(
    setLogoColor(color)
    ),
  colorSidebarHint: (color) => dispatch(
    setHintColor(color)
    ),
  setLogoState: (state) => dispatch(
    setLogoState(state)
    ),
  enableEventListenersForFutureAttempts: () => dispatch(
    enableHomeEventListeners()
  ),
  setSidebarState: (state) => dispatch(
    setSidebarState(state)
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(Home)