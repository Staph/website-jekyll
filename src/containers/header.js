'use strict'

import Vivus             from 'vivus';
import React             from 'react';
import HeaderComponent   from 'components/header'

class Header extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      textVisible: false
    }
  }
  componentDidMount(){
    this.logoAnimation = new Vivus( 'logo-header', {
      type: 'scenario',
      animTimingFunction: Vivus.EASE_IN
    });
  }
  componentWillReceiveProps( nextProps ){
    if ( this.props.initialLoad ) {
      var _this = this;
      _this.logoAnimation.play( 1, function(){
        setTimeout( () => {
          _this.setState({ textVisible: true })
          _this.logoAnimation.destroy()
        }, 500 );
      })
    }
    else{
      this.setState({textVisible: true})
    }
  }
  render(){
    return pug`
      HeaderComponent(textVisible=${ this.state.textVisible })
    `
  }
}

export default Header
