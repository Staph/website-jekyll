import React                 from 'react'
import forEach               from 'lodash.foreach'
import { colors }            from 'common/colors'
import { BrowserRouter }     from 'react-router-dom'
import { connect }           from 'react-redux'
import { setHintVisibility } from 'actions/sidebar'
import Sidebar               from 'containers/sidebar'
import Routing               from 'containers/routing'

class Root extends React.Component {
  constructor(props){
    super(props);

    this.state = {
        options: { initialLoad: true
                   },
        home:    { scrollTop: 0
                   }
    };
  }

  checkOverlap = (el, bgs) => {
    var elBox   = el.getBoundingClientRect(),
        overlap = false;

    forEach(bgs, (bg) => {
      var bgBox = bg.getBoundingClientRect();
      if(!(elBox.right  < bgBox.left  ||
           elBox.left   > bgBox.right ||
           elBox.bottom < bgBox.top   ||
           elBox.top    > bgBox.bottom)){
        overlap = true;
      }
    })

    return overlap;
  }

  showHeader = () => {
    this.setState({
      header: { ...this.state.header, visible: true }
    });
  }

  setHomeScroll = (project) => {
    this.setState({
      home: { ...this.state.home, scrollTop: project }
    });
  }

  initAnimation = () => {
    this.showHeader();

    setTimeout( () => {
      this.props.showSidebarHint(true);

      this.setState({
        options: { ...this.state.options, initialLoad: false }
      });
    }, 2000 );
  }

  render(){
    return (
      <BrowserRouter>
        <div>
          <Sidebar  />
          <Routing options={this.state.options}
                   home={this.state.home}
                   checkOverlap={this.checkOverlap}
                   initAnimation={this.initAnimation}
                   storeDomElements={this.storeDomElements}
                   setHomeScroll={this.setHomeScroll} 
                   sidebar={this.state.sidebar}/>
        </div>
      </BrowserRouter>
      )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  showSidebarHint: (visibility) => dispatch(
    setHintVisibility(visibility)
    )
})

export default connect(
  null,
  mapDispatchToProps
  )(Root)