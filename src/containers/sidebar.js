'use strict'

import forEach               from 'lodash.foreach'
import React                 from 'react'
import { visibilityFilter }  from 'app'
import { connect }           from 'react-redux'
import { withRouter }        from 'react-router-dom'
import SidebarComponent      from 'components/sidebar'
import { setLogoVisibility } from 'actions/sidebar'

const SCROLL_TO_NEXT_BLOCK_TRESHOLD = 0.4

class Sidebar extends React.Component {
  storeHomeDomElements = () => {
    this.sidebarHint = document.querySelector("#sidebar-hint-text");
    this.blocks      = document.querySelectorAll('.home-block');
  }

  storeProjectDomElements = () => {
    this.projectBody = document.querySelector('.project-body-content');
  }

  handleHintClickProject = () => {
    this.storeProjectDomElements();
    var viewportHeight = window.innerHeight;
    this.projectBody.scrollBy({ 
      top: viewportHeight * 0.75,
      left: 0, 
      behavior: 'smooth' 
    });
  }

  handleLogoClick = () => {
    switch(this.props.logoState) {
      case 'back':
        if (this.props.location.pathname != '/') {
          this.props.history.push('/')
        }
        break;
      case 'up':
        window.scrollTo({
          top: 0,
          behavior: "smooth"
        });
        break;
      default:
        break;
    }
  }

  currentOverlapping = () => {
    var elBox = this.sidebarHint.getBoundingClientRect();
    var overlappingEl = -1;
    forEach(this.blocks, function(block, index) {
      var blockBox = block.getBoundingClientRect();
      if(!(elBox.bottom < blockBox.top   ||
           elBox.top    > blockBox.bottom)){
        // find what part of block is visible
        var visisbleBlockPart = window.innerHeight - blockBox.top;
        var blockHeight = blockBox.bottom - blockBox.top
        if (
            visisbleBlockPart / blockHeight <
            SCROLL_TO_NEXT_BLOCK_TRESHOLD) {
          // if currently visisble less than SCROLL_TO_NEXT_BLOCK_TRESHOLD,
          // scroll to the end of current block
          overlappingEl = index - 1;
        } else {
          // scroll to the next block
          // if currently visible more than SCROLL_TO_NEXT_BLOCK_TRESHOLD
          overlappingEl = index;
        }
        return false;
      }
    });
    return overlappingEl;
  }

  handleHintClickHome = () => {
    this.storeHomeDomElements();
    var nextBlock = this.blocks[this.currentOverlapping() + 1];
    if( nextBlock ){
      nextBlock.scrollIntoView({
        behavior: 'smooth',
        block:    'start',
        inline:   'start'
      });
    }
  }

  handleHintClick = () => {
    switch(this.props.state) {
      case 'project':
        this.handleHintClickProject();
      case 'home':
        this.handleHintClickHome();
      default:
        break;
    }
  }

  render(){
    return (
      <SidebarComponent handleHintClick={ this.handleHintClick }
                        handleLogoClick={ this.handleLogoClick }
                        logoVisible={ this.props.logoVisible }
                        logoColor={ this.props.logoColor }
                        hintVisible={ this.props.hintVisible }
                        hintColor={ this.props.hintColor }
                        logoState={ this.props.logoState } />
      )
  }
}

export default connect(
  state => state.sidebar
  )(withRouter(Sidebar))
