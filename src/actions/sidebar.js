export const setLogoVisibility = (visibility) => {
  return { type: 'SIDEBAR_LOGO_VISIBILITY', visible: visibility }
}
export const setHintVisibility = (visibility) => {
  return { type: 'SIDEBAR_HINT_VISIBILITY', visible: visibility }
}
export const setLogoColor = (color) => {
  return { type: 'SIDEBAR_LOGO_COLOR', color: color }
}
export const setHintColor = (color) => {
  return { type: 'SIDEBAR_HINT_COLOR', color: color }
}

export const setLogoState = (rawState) => {
  const expected = [
    'default',
    'back',
    'up'
  ]
  var state = expected[1]

  if (expected.includes(rawState)) {
    state = rawState;
  }

  return { type: 'SIDEBAR_LOGO_STATE', state: state }
}

export const setSidebarState = (rawState) => {
  const expected = [
    'home',
    'project'
  ]
  var state = expected[0]

  if (expected.includes(rawState)) {
    state = rawState;
  }

  return { type: 'SET_SIDEBAR_STATE', state: state }
}


export const disableProjectDetailsEventListeners = () => ({
  type: 'DISABLE_PROJECT_DETAILS_EVENT_LISTENERS'
})

export const enableProjectDetailsEventListeners = () => ({
  type: 'ENABLE_PROJECT_DETAILS_EVENT_LISTENERS'
})

export const disableHomeEventListeners = () => ({
  type: 'DISABLE_HOME_EVENT_LISTENERS'
})

export const enableHomeEventListeners = () => ({
  type: 'ENABLE_HOME_EVENT_LISTENERS'
})
