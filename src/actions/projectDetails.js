export const rerenderProjectPageWithNewViewPort = () => ({
  'type': 'RERENDER_PROJECT_PAGE_WITH_NEW_VIEW_PORT'
})

export const disableRerendering = () => ({
  'type': 'DISABLE_RERENDEING'
})