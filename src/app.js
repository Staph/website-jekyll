'use strict';

import 'babel-polyfill'
import 'intersection-observer'
import smoothscroll         from 'smoothscroll-polyfill'

import   React              from 'react'
import   ReactDOM           from 'react-dom'
import { Provider }         from 'react-redux'
import { createStore,
         combineReducers,
         applyMiddleware }  from 'redux'
import   sidebarReducer     from 'reducers/sidebar'
import   projectDetails     from 'reducers/projectDetails'
import   Root               from 'containers/root'

smoothscroll.polyfill()

// ===================

let rootReducer = combineReducers({
  sidebar:        sidebarReducer,
  projectDetails: projectDetails,
})

let store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )

// ===================

ReactDOM.render((
  <Provider store={store}>
    <Root />
  </Provider>
  ),
  document.querySelector('#main')
)
