const initialState = {
  rerenderOnViewportChanged: false
}

const sidebar = (state = initialState, action) => {
  switch (action.type) {
    case 'RERENDER_PROJECT_PAGE_WITH_NEW_VIEW_PORT':
      return Object.assign({}, state, {
        rerenderOnViewportChanged: true
      })
    case 'DISABLE_RERENDEING':
      return Object.assign({}, state, {
        rerenderOnViewportChanged: false
      })
    default:
      return state
  }
}

export default sidebar