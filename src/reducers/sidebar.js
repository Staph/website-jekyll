import colors from 'common/colors'

const initialState = {
  logoVisible:                        false,
  logoColor:                          colors.dark,
  hintVisible:                        false,
  hintColor:                          colors.dark,
  logoState:                          'default',
  projectDetailEventListenersEnabled: true,
  homeEventListenersEnabled:          true,
  state:                              'home',
}

const sidebar = (state = initialState, action) => {
  switch (action.type) {
    case 'SIDEBAR_LOGO_VISIBILITY':
      return Object.assign({}, state, {
        logoVisible: action.visible
      })
    case 'SIDEBAR_HINT_VISIBILITY':
      return Object.assign({}, state, {
        hintVisible: action.visible
      })
    case 'SIDEBAR_LOGO_COLOR':
      return Object.assign({}, state, {
        logoColor: action.color
      })
    case 'SIDEBAR_HINT_COLOR':
      return Object.assign({}, state, {
        hintColor: action.color
      })
    case 'SIDEBAR_LOGO_STATE':
      return Object.assign({}, state, {
        logoState: action.state
      })
    case 'DISABLE_PROJECT_DETAILS_EVENT_LISTENERS':
      return Object.assign({}, state, {
        projectDetailEventListenersEnabled: false
      })
    case 'ENABLE_PROJECT_DETAILS_EVENT_LISTENERS':
      return Object.assign({}, state, {
        projectDetailEventListenersEnabled: true
      })
    case 'DISABLE_HOME_EVENT_LISTENERS':
      return Object.assign({}, state, {
        homeEventListenersEnabled: false
      })
    case 'ENABLE_HOME_EVENT_LISTENERS':
      return Object.assign({}, state, {
        homeEventListenersEnabled: true
      })
    case 'SET_SIDEBAR_STATE':
      return Object.assign({}, state, {
        state: action.state
      })
    default:
      return state
  }
}

export default sidebar