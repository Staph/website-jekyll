'use strict'

import React  from 'react';
import colors from 'common/colors';

const defaultColor = colors.dark;

export const IconLogo = ({ color = defaultColor }) => {
  return pug`
    svg(width="30px"
        height="30px"
        viewBox="0 0 30 30"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        className="logo"
        )
      g(stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
        )
        g(stroke=${ color }
          strokeWidth="2"
          )
          path(d="M30,1 L0,1")
          path(d="M0,10 L21,10")
          path(d="M30,20 L9,20")
          path(d="M0,29 L30,29")
    `
}

export const IconLeft = ({ color = defaultColor }) => {
  return pug`
    svg(width='24px',
        height='24px',
        viewBox='0 0 24 24',
        version='1.1',
        xmlns='http://www.w3.org/2000/svg',
        xmlnsXlink='http://www.w3.org/1999/xlink'
        )
      title Arrow Left
      g(stroke='none',
        strokeWidth='1',
        fill='none',
        fillRule='evenodd'
        )
        g
          path(d='M13,7.82842712 L13,20 L11,20 L11,7.82842712 L7.75735931,11.0710678 L6.34314575,9.65685425 L10.5857864,5.41421356 L12,4 L17.6568542,9.65685425 L16.2426407,11.0710678 L13,7.82842712 Z',
               fill=${ color },
               transform='translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000) '
               )
    `
}

export const IconClose = ({ color = defaultColor }) => {
  return pug`
    svg.project-body-close-icon(
      x="0px",
      y="0px",
      viewBox="0 0 30 30"
      )
      g(fill=${ color })
        path.st0(d="M20.4,2L28,9.6v10.8L20.4,28H9.6L2,20.4V9.6L9.6,2H20.4 M21.2,0H8.8L0,8.8v12.4L8.8,30h12.4l8.8-8.8V8.8L21.2,0\
        L21.2,0z")
        rect.st0(x="7", y="14", transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 36.2132 15)", width="16", height="2")
        rect.st0(x="7", y="14", transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 15 36.2132)", width="16", height="2")
    `
}