import React       from 'react'
import Header      from 'containers/header'
import Footer      from 'components/footer'
import AboutMe     from 'components/about-me'
import ProjectList from 'components/project/projectList'

const Home = ({ options, setHomeScroll }) => {
  return pug`
    div
      Header(options=options)
      AboutMe
      ProjectList(setHomeScroll=setHomeScroll)
      Footer
  `
}

export default Home