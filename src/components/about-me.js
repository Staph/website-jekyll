import React  from 'react'
import Button from 'components/button'
import Moment from 'moment'

const AboutMe = () => {
  let age = Moment().diff('1991-06-02', 'years', false);
  return pug`
    .about-me.fullscreen-limited.home-block
      .row.about-me-background
        .col-sm-6.col-xxl-4.about-me-photo-container
          img.about-me-photo(
            src="/assets/images/my-face.jpg"
            srcSet="/assets/images/my-face-253w.jpg 253w,\
                    /assets/images/my-face-506w.jpg 506w,\
                    /assets/images/my-face-758w.jpg 758w,\
                    /assets/images/my-face-1011w.jpg 1011w"
            )
        .col-xs-11.col-sm-6.col-xxl-5.col-xxl-offset-1.about-me-content
          h1.about-me-header Hi! My name is Vas.
          p.about-me-copy.
            I'm ${age} years old, I'm passionate about all things design, and
            currently I'm running with
            #[a(href="https://www.reaktor.com" target="_blank") Reaktor] and
            helping them bring the best possible digital products to life. 
            In my free time I enjoy avant-garde music,
            vegan food and supporting people. I’m passionate about creating
            design solutions that balance scalability for the needs of the
            business, personalization and lack of friction for the end user
            experience and creative use of tech.
          a.about-me-button(href="/assets/files/Vas_Zayarskiy_CV.pdf"
            target="_blank"
            )
            Button(label="Download my CV")

  `
}

export default AboutMe