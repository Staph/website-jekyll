import React       from 'react'
import ProjectHero from 'components/project/projectHero'

const renderProjectScope = (scope) => {
  let output = ''
  scope.forEach(function(el, i){
    output += el
    if (i != scope.length - 1) {
      output += ', '
    }
  })
  return output
}
const ProjectDetails = ({ project }) => {
  return pug`
    .project-body.showing(
      id=${ 'project-body-' + project.slug }
      style=${{ backgroundColor: project.detailsBg }}
      )
      .project-body-content
        ProjectHero(slug=${ project.slug }
                    srcSet=${ project.heroSrcSet })
        .project-body-text
          .project-body-meta-container
            h1.project-body-header ${ project.name }
            .row
              .col-xs-12.col-sm-6.project-body-excerpt.first-sm ${ project.excerpt }
              .col-xs-12.col-sm-5.col-sm-offset-1.first-xs
                table.project-body-meta
                  tbody
                    tr
                      td.h6.header-luzern Client:
                      td ${ project.client }
                    tr
                      td.h6.header-luzern Role:
                      td ${ project.role }
                    tr
                      td.h6.header-luzern Scope:
                      td ${ renderProjectScope(project.scope) }
          hr.project-body-meta-divider
          .project-body-copy ${ project.content }
    `
}

export default ProjectDetails