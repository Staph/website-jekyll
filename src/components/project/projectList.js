'use strict'

import   React               from 'react'
import   projectDefsList     from 'common/projectDefs'
import   ProjectListItem     from 'components/project/projectListItem'
import   ProjectHero         from 'components/project/projectHero'

const ProjectList = ({ setHomeScroll }) => {
  return pug`
    main#post-list
      .project-list
        ${ projectDefsList.map( ( projectDefs ) =>
          <ProjectListItem key={ 'project-' + projectDefs.slug }
                           project={ projectDefs }
                           setHomeScroll={ setHomeScroll } />
        )}
      .project-heroes.hidden
        ${ projectDefsList.map( ( projectDefs ) =>
          <ProjectHero key={ projectDefs.slug }
                       slug={ projectDefs.slug }
                       srcSet={ projectDefs.heroSrcSet } />
        )}
  `
}

export default ProjectList