import React   from 'react'
import forEach from 'lodash.foreach'

const ProjectHero = ({ slug, srcSet }) => {
  var srcSetParsed = '';

  if (srcSet) {
    forEach(srcSet, function(size){
      srcSetParsed = srcSetParsed + '/assets/' + slug + '/hero-' + size + '.jpg ' + size + ',';
    })
  }

  return pug`
    img.project-hero(
      src=${'/assets/' + slug + '/hero.jpg'}
      srcSet=${ srcSetParsed }
      )
  `
}

export default ProjectHero