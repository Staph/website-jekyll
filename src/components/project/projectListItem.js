import   React        from 'react'
import { withRouter } from 'react-router-dom'
import   Button       from 'components/button'

class ProjectListItem extends React.Component{
  handleClick = () => {
    let slug      = this.props.project.slug,
        scrollTop = window.scrollY;
    this.props.history.push(`/project/${slug}`);
    this.props.setHomeScroll(scrollTop);
  }

  render(){
    const project = this.props.project;
    return pug`
      .project.fullscreen-limited.home-block.column(
        key=project.slug
        style={ background: project.previewBg }
        data-project=project.slug
        className="theme-"+project.theme
                  +" project-"+project.slug
                  +(project.hidden ? ' hidden' : '')
        )
        .row.project-header-container
          h1.project-header ${ project.name }
        .row.project-text-container
          .col-sm-6.project-text-wrapper
            .box
              .project-text
                .project-excerpt
                  = project.excerpt
                .project-read-more-wrapper.text-right
                  span(onClick=${ this.handleClick })
                    Button(label="Read more")
          .col-sm-6.project-attachments
            = project.attachment
    `
  }
}

export default withRouter(ProjectListItem);