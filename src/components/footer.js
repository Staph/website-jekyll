'use strict'

import React from 'react';

class Footer extends React.Component {
  render(){
    return pug`
      footer#footer.container-fluid
        .site-footer.fullscreen.home-block.row.around-xs.middle-xs
          .box.site-footer-container.text-center
            .h6.header-luzern.footer-header Let's do stuff:
            a.celias.h4.footer-email(
              href="mailto:contact@vasily.cc"
              ) contact@vasily.cc
            .h6.header-luzern.footer-links
              a.footer-link(
                href="https://www.linkedin.com/in/zayarskiy/"
                ) LinkedIn
              span.footer-links-divider |
              a.header-luzern.footer-link(
                href="https://www.instagram.com/staph_a/"
                ) Instagram
          .site-footer-source.text-center.
            Lovingly handcrafted by passionate monkeys. Check source
            #[a(href="https://gitlab.com/Staph/website-jekyll" target="_blank") here]
      `
  }
}

export default Footer