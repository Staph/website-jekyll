'use strict'

import React  from 'react';
import colors from 'common/colors';

const Header = ({ textVisible }) => {
  return pug`
    header#header.container-fluid(role='banner')
      .site-header.page-block.row.around-xs.middle-xs.fullscreen
        .box.site-header-banner.text-center
          svg#logo-header.logo(
                   width='30px',
                   height='30px',
                   viewBox='0 0 30 30',
                   version='1.1',
                   xmlns='http://www.w3.org/2000/svg',
                   xmlnsXlink='http://www.w3.org/1999/xlink'
                   )
            g(stroke='none', strokeWidth='1', fill='none', fillRule='evenodd')
              g(stroke=${ colors.dark }, strokeWidth='2')
                path(d='M30,1 L0,1'
                     data-start="0"
                     data-duration="30"
                     )
                path(d='M0,10 L21,10'
                     data-start="60"
                     data-duration="15"
                     )
                path(d='M30,20 L9,20'
                     data-start="60"
                     data-duration="15"
                     )
                path(d='M0,29 L30,29'
                     data-start="0"
                     data-duration="30"
                     )
          h4#header-wordmark.celias.site-header-wordmark(
            className=${ textVisible ? '' : 'transparent' }
            ) Vas Zayarskiy
    `
}

export default Header
