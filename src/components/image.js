'use strict'

import React from 'react';

export default class Image extends React.Component {
  formatSrcSet = () => {
    let src = this.props.src
    let srcSet = this.props.srcSet
    let output = ''
    let fileName = src.slice(0, src.lastIndexOf('.'))
    let extension = src.slice(src.lastIndexOf('.')+1)
    if (Array.isArray(srcSet)) {
      srcSet.forEach(function(size,index){
        output += `${fileName}-${size}w.${extension} ${size}w`
        if (index < srcSet.length-1) {
          output += ', '
        }
      })
    }
    return output;
  }
  getFullSizeLink = () => {
    let src = this.props.src
    let srcSet = this.props.srcSet
    if (srcSet) {
      let fileName = src.slice(0, src.lastIndexOf('.'))
      let extension = src.slice(src.lastIndexOf('.')+1)
      let maxSize = Math.max(...srcSet)
      return `${fileName}-${maxSize}w.${extension}`
    } else {
      return src
    }
  }
  toLinkOrNotToLink = () => {
    if (this.props.video) {
      return pug`
        img(src=this.props.src
            alt=this.props.name
            title=this.props.name
            data-lazy-video=this.props.video
            srcSet=${this.formatSrcSet()})
      `
    } else {
      return pug`
        a.image-link(
          href=${this.getFullSizeLink()}
          target='_blank'
          )
          img(src=this.props.src
              alt=this.props.name
              title=this.props.name
              data-lazy-video=this.props.video
              srcSet=${this.formatSrcSet()})
      `
    }
  }

  render(){
    return pug`
      span.image
        ${this.toLinkOrNotToLink()}
        em.image-caption ${this.props.name}
    `
  }
}