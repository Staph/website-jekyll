'use strict'

import React       from 'react';
import SidebarLogo from 'components/sidebarLogo';
import SidebarHint from 'components/sidebarHint';

const Sidebar = ({
  handleLogoClick,
  handleHintClick,
  logoVisible,
  logoColor,
  hintVisible,
  hintColor,
  logoState
  }) => {
  return pug`
    #sidebar
      .sidebar
        .sidebar-column
          .sidebar-body
            .sidebar-logo(
              className=${ logoState }
              )
              SidebarLogo(
                onClick=${ handleLogoClick }
                visible=${ logoVisible }
                color=${ logoColor }
                state=${ logoState }
                )
            .sidebar-hint
              SidebarHint(
                nextBlock=${ handleHintClick }
                visible=${ hintVisible }
                color=${ hintColor }
                )
  `
}

export default Sidebar