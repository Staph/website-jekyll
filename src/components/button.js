'use strict'

import React from 'react';

const Button = ({ label }) => {
  return pug`
    span.btn
      .btn-border.btn-border-top
      .btn-border.btn-border-left
      .btn-border.btn-border-bottom
      .btn-border.btn-border-right
      .btn-label.h6.header-luzern ${ label }
  `
}

export default Button