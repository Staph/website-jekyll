import   React                   from 'react'
import { TransitionGroup,
         CSSTransition }         from 'react-transition-group'
import { Switch,
         Route }            from 'react-router-dom'
import   Home                    from 'containers/home'
import   ProjectDetails          from 'containers/project/projectDetails'

const Routing = ({ location, sidebar, ...props }) => {
  return pug`
    TransitionGroup
      CSSTransition(classNames="project-animate"
                    timeout=${700}
                    key=${location.key},
                    onExiting=${() => {
                      props.onCssTransitionExiting(
                        location.pathname,
                        props.history.location.pathname
                      );
                    }}
                    onExited=${() => {
                      props.onCssTransitionExited(
                        location.pathname,
                        props.history.location.pathname
                      );
                    }}
                    )
        Switch(location=${location})
          Route(exact
                path='/'
                render=${() =>
                  <div>
                    <Home {...props} />
                  </div>
                })
          Route(path='/project/:slug'
                render=${() =>
                  <div>
                    <ProjectDetails {...props} />
                  </div>
                })
  `
}

export default Routing