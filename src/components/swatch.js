'use strict'

import   React           from 'react'
import   chroma          from 'chroma-js'
import { round,
         formatPercent } from 'common/math'

const Swatch = ({ label, hex, rgb, hsl, cmyk }) => {
  let chromaColor = chroma(hex);
  let colorIsLight = chromaColor.luminance() > 0.5
  if(rgb === undefined){
    rgb = `rgb(${
             chromaColor.get('rgb.r')
           },${
             chromaColor.get('rgb.g')
           },${
             chromaColor.get('rgb.b')
         })`
  }
  if(hsl === undefined){
    hsl = `hsl(${
             round(chromaColor.get('hsl.h'), 0)
           },${
             formatPercent(chromaColor.get('hsl.s'), 0)
           },${
             formatPercent(chromaColor.get('hsl.l'), 0)
         })`
  }
  if(cmyk === undefined){
    cmyk = `${
             formatPercent(chromaColor.get('cmyk.c'), 0)
           }, ${
             formatPercent(chromaColor.get('cmyk.m'), 0)
           }, ${
             formatPercent(chromaColor.get('cmyk.y'), 0)
           }, ${
             formatPercent(chromaColor.get('cmyk.k'), 0)
         }`
  }
  return pug`
    div.swatch(
      style={backgroundColor: hex}
      class=colorIsLight ? 'theme-light' : 'theme-dark'
      )
      h6.swatch-main #[strong ${label || hex}]
      table
        tbody
          tr
            td rgb:
            td ${rgb}
          tr
            td hsl:
            td ${hsl}
          tr
            td cmyk:
            td ${cmyk}

  `
}

export default Swatch