import   React      from 'react'
import   lottie     from 'lottie-web'
import   colors     from 'common/colors'
import { IconLogo } from 'components/icons'

class LottieLogo extends React.Component {
  setAnimationDirection = (nextProps) => {
    if (['back','up'].indexOf(nextProps.state) != -1) {
      this.anim.setDirection(1)
    } else {
      this.anim.setDirection(-1)
    }
  }
  componentWillReceiveProps(nextProps){
    if (this.props.state != nextProps.state) {
      this.setAnimationDirection(nextProps)
      this.anim.play()
    }
  }
  componentDidMount(){
    this.anim = lottie.loadAnimation({
      container: document.querySelector('#sidebar-logo-lottie'),
      renderer: 'svg',
      loop: false,
      autoplay: false,
      path: '/assets/animations/sidebar-logo-arrow-dark.json'
    });
  }
  render(){
    return (
      <div className={ this.props.color == colors.dark ?
                                           'logo'      :
                                           'logo light' }
           id="sidebar-logo-lottie" />)
  }
}

const SidebarLogo = ({
  onClick,
  visible,
  color,
  state
  }) => {

  return pug`
    #sidebar-logo(
      className=${ visible ? '' : 'transparent' }
      onClick=${ onClick }
      )
      LottieLogo(
        state=${state}
        color=${color}
        )
  `
}

export default SidebarLogo;