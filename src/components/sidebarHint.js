import React        from 'react';
import { IconLeft } from 'components/icons';

const SidebarHint = ({
  nextBlock,
  visible,
  color
  }) => {
  return pug`
    h6#sidebar-hint-text.header-luzern.sidebar-hint-text(
      onClick=${ nextBlock }
      className=${ visible ? '' : 'transparent' }
      style=${{ color: color }}
      )
      .sidebar-hint-text-icon
        IconLeft(color=${ color })
      span scroll
  `
}

export default SidebarHint
