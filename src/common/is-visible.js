'use strict'

import forEach from 'lodash.foreach';

var defaultOptions = {
  threshold: .2
}

export function revealElement(targets, options = defaultOptions){

  // Use like this:
  // revealElement(document.querySelectorAll('.reveal'));

  var revealElementCallback = function(entries, observer) {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        entry.target.classList.add('reveal-visible');
      }
    });
  };

  var observer = new IntersectionObserver(revealElementCallback, options);
  targets.forEach(target => {
    observer.observe(target);
  });

}


export function lazyLoadVideo(targets, options = defaultOptions){

  // Use like this:
  // lazyLoadVideo(document.querySelectorAll('[data-lazy-video]'));

  var lazyLoadVideoCallback = function(entries, observer) {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        var video  = document.createElement('video');
        var target = entry.target;
        video.setAttribute('src',         entry.target.getAttribute('data-lazy-video'));
        video.setAttribute('loop',        'true');
        video.setAttribute('autoplay',    'true');
        video.setAttribute('muted',       'true');
        video.setAttribute('playsinline', '');
        target.parentNode.insertBefore(video, target);
        target.remove();
      }
    });
  };

  var observer = new IntersectionObserver(lazyLoadVideoCallback, options);
  targets.forEach(target => {
    observer.observe(target);
  });

}


export function whenVisible(targets, callback, options = defaultOptions){

  // Use like this:
  // whenVisible(document.querySelectorAll('.reveal'), function(entries){
  //   console.log(entries);
  // });

  var isVisibleCallback = function(entries, observer){
    callback(entries);
  };
  var observer = new IntersectionObserver(isVisibleCallback, options);
  targets.forEach(target => {
    observer.observe(target);
  });

}