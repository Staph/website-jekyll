'use strict'

import React  from 'react'
import Swatch from 'components/swatch'
import Image  from 'components/image'

const holviDesign = {
  name:       'Holvi Design Language',
  slug:       'holvi-design',
  theme:      'light',
  previewBg:  '#eceef2',
  detailsBg:  '#eceef2',
  hidden:     false,
  client:     'Holvi Payment Services',
  role:       'UI Lead',
  scope:     ['Visual Design',
              ],
  heroSrcSet:['491w',
              '655w',
              '1310w',
              '1474w',
              '1965w',
              '2620w'
              ],
  excerpt:    pug`
      span
        p.
          Holvi Design Language is a visual design and interaction pattern
          system that strives to make every day design faster, more accessible
          to non-designers, all while maintaining a high quality of produced
          deliverables.
    `,
  content:    pug`
      span
        .row
          .col-xs-12.col-sm-6
            h2.project-body-copy-first#brief Brief
            p.
              Create a design language that is extensible and stresses
              foundational principles, rather than ready-made answers. Provide
              recipes that are easy to reuse by everybody on the team. Create a
              language that is fluid and adaptable enough to tackle emerging
              challenges and retain consistency across the board.

        .project-body-copy-block
          h2#grids Grids
          h4#baseline-grid Baseline grid
          .row
            .col-xs-12.col-sm-6
              p.
                The basis for all the units in Holvi UI is the baseline grid.
                Ensuring the elements stick to it is one of the basic
                fundamental principles of keeping the UI feeling visually clean
                and tidy, and it also helps approach visual design in a more
                systematic fashion.
              p.
                The baseline grid pitch is #[strong 4pt]. What this means in
                practice, is all the line heights are measured in multiples of
                4pt, e.g. 8pt, 12pt, 16pt, 20pt etc.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi-design/baseline-grid.png'
                    name='Text aligned to baseline grid')

        .project-body-copy-block
          h4#layout-grid Layout grid
          .row
            .col-xs-12.col-sm-6
              p.
                Layout grid is similar to baseline grid, and is based on the
                baseline grid, but it differs in one important way: while the
                baseline grid consists only of horizontal lines, layout grid
                introduces vertical lines.
              p.
                Another important distinction is the layout grid pitch is twice
                the size of the baseline grid, standing at #[strong 8pt].
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi-design/layout-grid.png'
                    name='Elements aligned to the layout grid')

        .project-body-copy-block
          h2#typography Typography
          .row
            .col-xs-12.col-sm-6
              p.
                Holvi design language uses Cartograph Sans as its main typeface,
                with Cartograph Mono serving as a monospace sibling. It has a
                strong personality, borrowing the utilitarian aesthetic of
                computer terminal and merging it with humanistic forms and
                readability. For longer bodies of text, Rational Sans is used.

        .project-body-copy-block
          h4#sizes Type sizes
          .row
            .col-xs-12.col-sm-6
              p.
                Typography dimensions in Holvi Design Language are constructed
                entirely parametrically, with modular scale used to determine
                type sizes. #[strong 14pt] is used as a base and #[strong 1.25]
                as the ratio to produce a harmonious set of type sizes. Modular
                scale has JS and SASS libraries that one could use to make the
                implementation stage of typography easier and ensure the end
                result is more maintainable.

        .project-body-copy-block
          h4#leading Leading (line height)
          .row
            .col-xs-12.col-sm-6
              p.
                Leading, or line height of text styles is derived by multiplying
                the point size of the text style by 1.2 and rounding it up to
                the nearest baseline increment (multiples of 4pt). The end
                result of this approach are lines of text that are in harmony
                with the rest of text and UI elements, with the same vertical
                rhythm.

        .project-body-copy-block
          h4#tracking Tracking (letter spacing)
          .row
            .col-xs-12.col-sm-6
              p.
                Tracking, or letter-spacing is calculated in rough accordance to
                this equation:
              p
                code.
                  tracking = −14.45065 + (14.11991 − −14.45065)/(1 + (point_size/2805.502)^0.003720551)
              p.
                The end result of entirely parametrically defined typographic
                system is the ability to easily extend and port the typography
                across multiple platforms without much effort.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi-design/type-tracking-curve.png'
                    name='Curve-fit plot of point size to tracking')

        .project-body-copy-block
          Image(src='/assets/holvi-design/typography2.png'
                name='Defined text styles')

        .project-body-copy-block
          h2#color Color
          h4#brand-colors Brand colors
          Swatch(hex='#F6FF6C')
          Swatch(hex='#FF5F0E')
          Swatch(hex='#064545')
          Swatch(hex='#BAE4EE')

        .project-body-copy-block
          h4#color-schemes Color schemes
          .row
            .col-xs-12.col-sm-6
              p.
                The brand colors are grouped into dark and light colors, with
                dark-light color pairings combined into color schemes.
          .row
            .col-xs-12.col-sm-6
              h6 Light colors
              p
                Swatch(hex='#F6FF6C')
                Swatch(hex='#BAE4EE')
            .col-xs-12.col-sm-6
              h6 Dark colors
              p
                Swatch(hex='#FF5F0E')
                Swatch(hex='#064545')
          p
            Image(src='/assets/holvi-design/color-schemes.jpg'
              srcSet=${[
                750,
                1500,
                2250,
                3000
                ]}
              name="Color schemes"
              )

        .project-body-copy-block
          h2#iconography Iconography
          .row
            .col-xs-12.col-sm-6
              p.
                Holvi icons are designed using a #[strong 24pt] grid, which
                means they fit precisely into the general grids used throughout
                the UI, while staying pixel perfect on lower resolution screens.
              p.
                The icon grid is loosely based on Material Design icon grid, and
                the icons are composed of basic shapes like rectangles, arrows
                and lines.
              p.
                The icon set complements the overall robust visual aesthetic
                with its 45 degree angles, sharp corners and relatively thick
                lines.

        .project-body-copy-block
          .row
            .col-xs-12.col-md-6
              p
                Image(src='/assets/holvi-design/icon-grid.png'
                      name='Icons align precisely to the grid')
            .col-xs-12.col-md-6
              p
                Image(src='/assets/holvi-design/icons-large.png'
                      name='Resulting icons')

        .project-body-copy-block
          Image(src='/assets/holvi-design/icons.png'
                name='Full icon set')

        .project-body-copy-block
          h2#layout Layout
          .row
            .col-xs-12.col-sm-6
              p.
                Holvi Design Language uses modular grid to lay out elements and
                views. For mobile, a 6 column layout is used, while on desktop,
                12 columns are preferred. Designers are encouraged to break the
                grid, in order to avoid unnecessary rigidity.
              p.
                Holvi Design Language is mobile-first and responsive, so
                designers should take extra care their designs scale up as well
                as down.

        .project-body-copy-block
          h2#in-use Design language in use
          p
            Image(src='/assets/holvi/cards.jpg'
                  srcSet=${[
                    1500,
                    1980,
                    3000,
                    6000
                    ]}
                  )
          p
            Image(src='/assets/holvi-design/desktop-registration.jpg'
                  srcSet=${[
                    800,
                    1600,
                    2400,
                    3200
                    ]}
                  )
    `,
}

const holviApps = {
  name:       'Holvi Mobile Apps',
  slug:       'holvi',
  theme:      'light',
  previewBg:  '#F6FF6C',
  detailsBg:  '#F6FF6C',
  client:     'Holvi Payment Services',
  role:       'Design Team Lead',
  scope:     ['Mobile App Design',
              'Digital Product Design'
              ],
  heroSrcSet:['1500w',
              '1980w',
              '3000w',
              '6000w'
              ],
  attachment: pug`
      .project-holvi-video-wrapper
        video.project-holvi-video(
          src="/assets/holvi/holvi-video.mov"
          loop="true"
          autoPlay="true"
          muted="true"
          playsInline="playsinline"
          )
  `,
  excerpt:    pug`
      span
        p.
          Holvi mobile apps are a one stop shop for running your business,
          including making and receiving payments, invoicing, managing your
          payment cards, storing your transaction receipts, preparing your
          account bookkeeping for your accountant and much more.
        p.
          During this project, Holvi transformed into a mobile-first company
          with a solid design process, a design language, restructured
          information architecture, a more organized onboarding flow and a
          host of other improvements.
    `,
  content:    pug`
      span
        .row
          .col-xs-12.col-sm-6
            h2.project-body-copy-first#brief Brief
            p.
              Come up with ways to let the small business owners focus on actually
              running their business and not on the day-to-day grind of receipt
              management, bookkeeping, tracking invoices and all other “overhead”
              of running your own business.

        .project-body-copy-block
          h2#process Process
          .row
            .col-xs-12.col-sm-6
              p The project was executed in several stages:
              h4#customer-personas Customer personas
              p.
                As a first order of priority, the customer persona generation process
                was kicked off inside the company. This process took existing customer
                base and market research data — among other things — to construct
                a few personas that would serve as a shorthand for describing the
                aggregate needs of the target audience.
              p The outcomes of this stage were two personas:
              table
                thead
                  tr
                    th(style=${{textAlign: 'left'}}) Kate
                    th(style=${{textAlign: 'left'}}) Tim
                tbody
                  tr
                    td
                      img(src='/assets/holvi/kate.png',
                          alt='Kate',
                          title='Kate')
                    td
                      img(src='/assets/holvi/tim.png',
                          alt='Tim',
                          title='Tim')
                  tr
                    td.
                      Kate is a #[strong graphic designer]
                      who sells her knowledge and skills as a service, uses mostly
                      invoicing and payment cards. Her knowledge of business finance
                      management is something she hasn’t had the time or passion
                      to improve.
                    td.
                      Tim is a #[strong yoga teacher] who has yoga as both his passion
                      and source of income. He has a legally mandated insurance, he
                      uses calendar, time tracking and email when dealing with his
                      customers. His business finance management skills are on par
                      with Kate.

        .project-body-copy-block
          h4#mvp-definition MVP Definition
          .row
            .col-xs-12.col-sm-6
              p.
                During this stage, a research has been conducted on existing user
                base, aiming to figure out which features of Holvi they would like
                to take on the go. As a result, the feature set defining the minimum
                implementation got locked in as follows:
              ul
                li.
                  #[strong Login] flows
                li.
                  #[strong Registration] and #[strong Customer Identity Verification]
                li.
                  #[strong Account Listing] and #[strong Balances]
                li.
                  #[strong Card Transaction Receipt Management]
                  and #[strong Pre-bookkeeping]
                li.
                  #[strong Transaction List]

        .project-body-copy-block
          h4#build Build
          .row
            .col-xs-12.col-sm-6
              p.
                The first step of the build stage was coming up with IA (information
                architecture) for the apps that would serve the needs of
                the customers, be as immediately understandable as possible, and
                be able to support scaling the functionality of the apps. This — as
                well as the subsequent flow definition stop — was done with the help
                of context scenarios.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi/hierarchy.jpg'
                    name='Information architecture')

        .project-body-copy-block
          .row
            .col-sm-6
              p.
                With the IA foundations laid down, first flows were designed in
                a process chart format, connecting the dots in the IA.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi/payment-flow.png'
                    name='Payment flow diagram'
                    srcSet=${[
                      2160,
                      3240,
                      4320
                      ]}
                    )

        .project-body-copy-block
          .row
            .col-xs-12.col-sm-6
              p.
                The wireframe stage of the initial MVP concerned itself with taking
                the flows a bit more forward towards locking in the more tactile
                aspects of interaction design, as well as getting common components
                in place to be reused later on.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi/payment-wireframes.png',
                    name='Payment wireframes')

        .project-body-copy-block
          .row
            .col-xs-12.col-sm-6
              p.
                A step beyond wireframes, developing the design language aided both
                the design and implementation efforts in keeping things consistent,
                scalable and agile — less stuff to design for each screen means
                faster screen design and more consistent implementation in the hands
                of the end users. Eventually, with the aid of design language,
                the wireframing stage got all but removed from the process,
                screen design happened right after process charts.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi/design-language.jpg'
                    name='Design language elements'
                    srcSet=${[
                      1577,
                      2082,
                      3154,
                      6308
                      ]}
                    )

        .project-body-copy-block
          .row
            .col-xs-12.col-sm-6
              p.
                Finally, the initial MVP took the shape the team felt comfortable
                enough with, and it was time to proceed with the closed beta stage.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/holvi/payment-flow-ui.jpg'
                    name='Payment flow UI'
                    video='/assets/holvi/payment-flow-ui.mp4'
                    )

        .project-body-copy-block
          h4#closed-beta Closed beta
          .row
            .col-xs-12.col-sm-6
              p.
                The closed beta started with selecting a subset of eligible customers
                who both fell under an early adopter archetype and were likely to be
                satisfied with the narrowed down scope of the application.
                Their feedback served as a guidance for iterating on the app features.

        .project-body-copy-block
          h4#soft-launch Soft launch
          .row
            .col-xs-12.col-sm-6
              p.
                Austrian soft launch as a way to test the app with non-early-adopter
                audience. Result: requested more features.
          .row
            .col-xs-12.col-sm-6
              p.
                Quite early on, we’ve found out that our initial assumption about the
                feature set required for an MVP was off base:
                the feedback from the customers was that #[strong invoicing]
                and #[strong payment card management]
                were an integral part of what customers expected from our application.
            .col-xs-12.col-sm-5.col-sm-offset-1
              p
                Image(src='/assets/holvi/invoicing.jpg'
                      name='Invoicing UI'
                      srcSet=${[
                        1312,
                        1732,
                        2625,
                        5249
                        ]}
                      )
              p
                Image(src='/assets/holvi/cards.jpg'
                      name='Payment cards UI'
                      srcSet=${[
                        1500,
                        1980,
                        3000,
                        6000
                        ]}
                      )

        .project-body-copy-block
          .row
            .col-xs-12.col-xs-12
              h2#product-illustrations Product illustrations
              p
                Image(src='/assets/holvi/payment-swiping.jpg',
                      name='Payment Swiping',
                      video='/assets/holvi/payment-swiping.mp4')
              p
                Image(src='/assets/holvi/cards-swiping.jpg',
                      name='Cards Swiping',
                      video='/assets/holvi/cards-swiping.mp4')
              p
                Image(src='/assets/holvi/payment-itemization.jpg',
                      name='Payment Itemization',
                      video='/assets/holvi/payment-itemization.mp4')
  `
}

const parallines = {
  name:       'Parallines',
  slug:       'parallines',
  theme:      'dark',
  previewBg:  '#00aeef',
  detailsBg:  '#00aeef',
  client:     'Parallines',
  role:       'Design Team Lead',
  scope:     ['Visual Identity Design',
              'Logo Design'
              ],
  heroSrcSet: ['1625w',
               '2145w',
               '3250w',
               '6500w'
              ],
  attachment: pug`
      img.project-home-image.project-parallines-logo-image(
        src="/assets/parallines/logo-white.svg"
        )
  `,
  excerpt:    pug`
      span
        p.
          Parallines is a creative studio that specializes in visual identity
          design, web design, web development, UI/UX and more.
        p.
          For the studio, I created a visual identity that included a logo,
          typography and distinctive graphic elements. The visual identity is
          applied to a variety of media, such as business cards, promotional
          materials and the studio website.
    `,
  content:    pug`
      span
        h2.project-body-copy-first#brief Brief
        .row
          .col-xs-12.col-sm-6
            p Create a stand-out visual identity for a small design firm.


        .project-body-copy-block
          h4#logo Logo
          .row
            .col-xs-12.col-sm-6
              p.
                The core of the Parallines visual identity is the logo, which is
                comprised of parallel stripes shaped into a letter P for Parallines
                and tilted to create a more balanced look. The width of the gaps in
                between the stripes matches the width of the stripes themselves.
              p.
                When displayed at small sizes, the logo fills the gaps between the
                stripes to preserve legibility.
              p.
                The precisely constructed nature of the logo aims to convey the way
                the studio tackles the problems — methodically and rationally.
            .col-sm-5.col-sm-offset-1
              p
                Image(src='/assets/parallines/logo-circle.svg'
                      name='Parallines logo'
                      )

        .project-body-copy-block
          h4#website Website
          .row
            .col-xs-12.col-sm-6
              p.
                The website is built using the WordPress content management system
                and is designed to allow the company to showcase their projects and
                client testimonials.
              p.
                The pages on the website are fully modular, and are constructed
                entirely without leaving the WordPress admin panel using pre-defined
                building blocks with custom content.
              p.
                The layout is designed to be fully responsive and suitable for
                devices starting at 320px wide and up. The color scheme of the
                website is generated randomly on page load within set parameters.
            .col-xs-12.col-sm-5.col-sm-offset-1
              p
                Image(src='/assets/parallines/website.jpg'
                      name='Website'
                      srcSet=${[
                        1700,
                        2244,
                        3400,
                        6800
                        ]}
                      )

        .project-body-copy-block
          h4#identity-applications Identity applications
          p
            Image(src='/assets/parallines/cup.jpg'
                  name='Paper cup'
                  )
          p
            Image(src='/assets/parallines/bc.jpg'
                  name='Business Card'
                  srcSet=${[
                    625,
                    825,
                    1250,
                    2500
                    ]}
                  )
          p
            Image(src='/assets/parallines/shirt.jpg'
                  name='Shirt'
                  srcSet=${[
                    375,
                    495,
                    750,
                    1500
                    ]}
                  )
  `
}

const berries = {
  name:       'Smoothie Berries',
  slug:       'smoothie-berries',
  theme:      'light',
  previewBg:  '#E2F4FC',
  detailsBg:  '#E2F4FC',
  client:     'VEK LLC',
  role:       'Visual Designer',
  scope:     ['Visual Identity Design',
              'Packaging Design'
              ],
  heroSrcSet: ['1625w',
               '2145w',
               '3250w',
               '6500w'
              ],
  excerpt:    pug`
      span
        p.
          Smoothie berries is a brand of locally-sourced, flash frozen berries
          sold in St. Petersburg, Russia.
        p.
          The product line consists of three sorts of berries: blueberries,
          lingonberries and blackcurrant, all native to the St. Petersburg area.
          All berries are distributed in a flash-frozen state and are locally
          produced with a lot of attention being paid to using the least
          possible amount of pesticides and other potentially harmful chemicals.
    `,
  content:    pug`
      span
        h2.project-body-copy-first#brief Brief
        .row
          .col-xs-12.col-sm-6
            p.
              Design a visual identity and apply it to packaging for the frozen
              berries brand that would be attractive to the target demographic of
              health-conscious young people.
            p.
              Visual identity of the packaging must serve as a base for the future
              development of the brand identity and the defining stylistic elements
              must be suitable for use in other media.

        .project-body-copy-block
          h2#process Process

        .project-body-copy-block
          h4#research Research
        .project-body-copy-block
          h6#target-audience Target audience
          .row
            .col-xs-12.col-sm-6
              p.
                Firstly, the project was kicked off with a research stage: together
                with the client, the target demographic of mostly young people who are
                aware of current trends, are health-conscious, probably into sports
                and generally lead an active lifestyle was locked in based on the
                market research.

        .project-body-copy-block
          h6#packaging-type Packaging type
          .row
            .col-xs-12.col-sm-6
              p.
                Packaging solution was chosen based on cost, durability of the
                packaging, ease of procurement and thermal insulation properties.
                The .5l Tetra Rex packaging solution by Tetra Pak was identified as
                the superior solution based on those criteria.
              p.
                This thermally insulated packaging provides extended product life
                outside the freezer, and gives added protection to the products as
                they are often mishandled during transportation and storage. With this
                packaging, the product stays at a temperature below melting point for
                about two hours depending on the initial product and outside
                temperatures. Keeping the berries frozen before they reach the client
                is crucial because refreezing it will severely damage the product.
                Compared to plastic bags, the packaging also gives berries moderate
                protection against physical shock and other possible rough conditions
                during distribution, so it is more likely to be in best condition
                possible when it reaches the consumer.
              p.
                In the shops, the product will be stored in freezers of two major
                types: front- and top-loaded, so the packaging should be clearly
                dentifiable from both perspectives.

        .project-body-copy-block
          h6#competitor-analysis Competitor analysis
          .row
            .col-xs-12.col-sm-6
              p.
                After getting an estimation of the ballpark price point of the product
                once it hits the market, it was possible to figure out the
                competition, and it was determined that four brands fall into a
                similar price range.
              p Uniting features among these competing products are:
              ul
                li The base packaging, which is a basic plastic bag;
                li Prominent display of actual berries;
                li Heavy reliance on photographic imagery or imitation of photographic imagery.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/smoothie-berries/competitors.jpg'
                    name='Competing brands'
                    )

        .project-body-copy-block
          h6#production-process Production process
          .row
            .col-xs-12.col-sm-6
              p.
                The Flexo process is a type of letterpress process that uses rubber
                or photopolymer printing plates and fast-drying low-viscosity inks to
                print on paper or other substrates, it consists of an ink supply, an
                anilox roller, a soft printing plate and finally a printing substrate.
                The process transfers the low-viscosity ink from a supply to the
                anilox roller, which transfers it to a printing plate with the help
                of the indentations on its surface. The ink is then transferred from
                the printing plate to the substrate that is located on the impression
                cylinder.
            .col-xs-12.col-sm-5.col-sm-offset-1
              Image(src='/assets/smoothie-berries/flexography.png'
                    name='Flexography diagram'
                    )

        .project-body-copy-block
          .row
            .col-xs-12.col-sm-6
              p.
                Tetra Pak offers two quality levels, which they call Flexo Line
                (client’s choice) and Flexo Process. In the Flexo Line method all
                colors used should be treated as spot colors not process colors, and
                the process will only allow two colors to be overprinted, with only
                one of them able to be screened.
              p.
                Effectively, this means that the only artwork allowed on the packaging
                is geometric/line drawings, as no gradients or photography is possible
                with these limitations.

        hr

        .project-body-copy-block
          h4#design Design

        .project-body-copy-block
          h6#logo Logo
          .row
            .col-xs-12.col-sm-6
              Image(src='/assets/smoothie-berries/logo-ideation.png'
                    name='Some initial logo design exploration'
                    )
            .col-xs-12.col-sm-6
              Image(src='/assets/smoothie-berries/logo-ideation-final.jpg'
                    name='Sketch of the final logo version'
                    )

        .project-body-copy-block
          h6#packaging Packaging
          p
            Image(src='/assets/smoothie-berries/berries-patterns.png'
                  name='Pattern sketches'
                  )
          p
            Image(src='/assets/smoothie-berries/packaging.jpg'
                  name='Final result'
                  )
          h2#brand-visuals-applications Brand visuals applications
          p
            Image(src='/assets/smoothie-berries/website.jpg'
                  name='Website'
                  )
          p
            Image(src='/assets/smoothie-berries/billboard.jpg'
                  name='Billboard ad'
                  )
  `
}

const projectDefsList = [
  holviApps,
  holviDesign,
  parallines,
  berries
]

export default projectDefsList