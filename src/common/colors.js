'use strict'

const colors = {
  light: '#f9f9f9',
  dark:  '#595959'
}

export default colors